import { Movie } from '../interfaces/movie';
import { Rating } from '../interfaces/rating';

const rating: Rating[] = [{ source: 'a', value: 'a' },{ source: 'a', value: 'a' }]

export const films: Movie[] = [
 {
    title: 'The Toxic Avenger',
    year: 1984,
    _yearData: '1984',
    rated: 'R',
    released: '1986-04-10T22:00:00.000Z',
    runtime: '82 min',
    genres: 'Action, Comedy, Horror, Sci-Fi',
    director: 'Michael Herz, Lloyd Kaufman',
    writer: 'Lloyd Kaufman (story), Joe Ritter (screenplay), Lloyd Kaufman (additional material), Gay Partington Terry (additional material), Stuart Strutin (additional material)',
    actors: 'Andree Maranda, Mitch Cohen, Jennifer Babtist, Cindy Manion',
    plot: 'This is the story of Melvin, the Tromaville Health Club mop boy, who inadvertently and naively trusts the hedonistic, contemptuous and vain health club members, to the point of accidentally ending up in a vat of toxic waste. The devastating results then have a transmogrification effect, his alter ego is released, and the Toxic Avenger is born, to deadly and comical results. The local mop boy is now the local Superhero, the saviour of corruption, thuggish bullies and indifference. Troma classic with good make-up effects and stunts, a pleasant surprise indeed.',
    languages: 'English',
    country: 'USA',
    awards: '1 nomination.',
    poster: 'https://m.media-amazon.com/images/M/MV5BNzViNmQ5MTYtMmI4Yy00N2Y2LTg4NWUtYWU3MThkMTVjNjk3XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg',
    metascore: '42',
    rating: 6.3,
    votes: '24,888',
    id: 'tt0090190',
    type: 'movie',
    dvd: undefined,
    boxoffice: 'N/A',
    production: 'Troma Entertainment Inc.',
    website: 'N/A',
    name: 'The Toxic Avenger',
    series: false,
    imdburl: 'https://www.imdb.com/title/tt0090190'
 }
];
