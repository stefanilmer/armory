import { Component, OnInit, Input } from '@angular/core';
import { Movie } from '../../interfaces/movie';

@Component({
  selector: 'app-movieModel',
  template: `
    <table class="table table-sm table-bordered">
      <tbody>
        <tr class="table-primary">
          <th scope="row" style="width: 16.66%">Naam</th>
          <td>
            <strong>{{ Movie.id }} {{ Movie.title }}</strong>
          </td>
        </tr>
        <tr>
          <th scope="row">poster</th>
          <td><img src="{{ Movie.poster }}" alt="{{ Movie.title }}"></td>
        </tr>
        <tr>
          <th scope="row">Actors</th>
          <td>{{ Movie.actors }}</td>
        </tr>
        <tr>
          <th scope="row">Plot</th>
          <td>{{ Movie.plot }}</td>
        </tr>
        <tr>
          <th scope="row">Ratings</th>
          <td>
            <ol>
              <li *ngFor="let rating of Movie.ratings">{{ rating }}</li>
            </ol>
          </td>
        </tr>
      </tbody>
    </table>
  `,
})
export class MovieComponent implements OnInit {
  @Input() movie: Movie;

  constructor() {}

  ngOnInit(): void {
    console.log('Movie Component onInit');
  }
}
