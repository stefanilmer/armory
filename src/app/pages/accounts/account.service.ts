import { Injectable } from '@angular/core'
import { Observable, BehaviorSubject, throwError } from 'rxjs'
import { map, tap, retry, catchError } from 'rxjs/operators'
import { Account } from './account.model'
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { environment } from 'src/environments/environment'

@Injectable({
  providedIn: 'root'
})
export class AccountService {
    accounts: Account[]

  accountsAvailable = new BehaviorSubject<boolean>(false)

  constructor(private http: HttpClient) {
    console.log('AccountsService constructed')
    console.log(`Connected to ${environment.ApiUrl}`)
  }

  public getAccounts(): Observable<Account[]> {
    console.log('getAccounts')
    return this.http.get<ApiResponse>(`${environment.ApiUrl}/api/users?offset=0&amount=8`).pipe(
      //   convert incoming responsestring to json
      // map(response => response.json()),
      //   get only the results property
      // map(response => response.results),
      //   optionally log the results
      // tap(console.log),
      //   convert json array to User array
      // map(users => users.map(data => new User(data))),
      //   optionally log the results
      // tap(console.log)
      catchError(this.handleError), // then handle the error
      // tap( // Log the result or error
      //   data => console.log(data),
      //   error => console.error('NU HIER: ' + error)
      // ),
      // all of the above can also be done in one operation:
      map(response => response.results.map(data => new Account(data))),
      tap(accounts => {
        this.accounts = accounts
        this.accountsAvailable.next(true)
      })
    )
  }

  getAccount(id: number): Account {
    console.log(`getAccount(${id})`)

    if (this.accounts && id >= 0 && id < this.accounts.length) {
        return this.accounts[id]
    } else {
        return undefined
    }
  }

  createAccount(account: Account) {
    console.log('createAccount')
    return this.http.post<ApiResponse>(`${environment.ApiUrl}/api/users`, account).pipe(
      catchError(this.handleError), // then handle the error
      tap(
        // Log the result or error
        data => console.log(data),
        error => console.error(error)
      )
    )
  }

  updateAccount(account: Account) {
    console.log('updateAccount', account)
    return this.http
      .put<ApiResponse>(`${environment.ApiUrl}/api/users/${account.id}`, account)
      .pipe(
        catchError(this.handleError), // then handle the error
        tap(
          data => console.log(data)
          // ,error => console.error(error)
        )
      )
      .subscribe(result => {
        console.log('updateAccount returned', result)
        this.accounts.push(account)
        this.accountsAvailable.next(true)
      })
  }

  private handleError(error: HttpErrorResponse) {
    console.log('handleError')
    // if (error.error instanceof ErrorEvent) {
    //   // A client-side or network error occurred. Handle it accordingly.
    //   console.error('An error occurred:', error.error.message);
    // } else {
    //   // The backend returned an unsuccessful response code.
    //   // The response body may contain clues as to what went wrong,
    //   console.error(
    //     `Backend returned code ${error.status}, ` +
    //     `body was: ${error.message}`);
    // }

    // return an observable with a user-facing error message
    return throwError(
      // 'Something bad happened; please try again later.'
      error.message || error.error.message
    )
  }
}

/**
 * This interface specifies the structure of the expected API server response.
 */
export interface ApiResponse {
  results: any[]
}