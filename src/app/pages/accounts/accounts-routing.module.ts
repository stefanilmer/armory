import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { AccountsComponent } from './accounts.component'
import { AccountListComponent } from './account-list/account-list.component'
import { AccountDetailComponent } from './account-detail/account-detail.component'
import { AccountNotfoundComponent } from './account-notfound/account-notfound.component'
import { AccountEditComponent } from './account-edit/account-edit.component'
import { AccountProfileComponent } from './account-profile/account-profile.component'

const routes: Routes = [
  { path: 'accounts/me', component: AccountProfileComponent },
  { path: 'accounts/list', component: AccountListComponent },
  {
    path: 'accounts/list/new',
    component: AccountEditComponent,
    data: {
      userAlreadyExists: false,
      title: 'New Account'
    }
  },
  { path: 'accounts/list/:id', component: AccountDetailComponent },
  {
    path: 'accounts/list/:id/edit',
    component: AccountEditComponent,
    data: {
      userAlreadyExists: true,
      title: 'Edit Accounts'
    }
  },
  {
    path: 'accounts',
    component: AccountsComponent,
    children: [
      { path: '', component: AccountDetailComponent },
      {
        path: 'new',
        component: AccountEditComponent,
        data: {
          userAlreadyExists: false,
          title: 'New User'
        }
      },
      { path: ':id', component: AccountDetailComponent },
      {
        path: ':id/edit',
        component: AccountEditComponent,
        data: {
          userAlreadyExists: true
        }
      },
      { path: '**', component: AccountNotfoundComponent }
    ]
  }
]

@NgModule({
  imports: [
    // Always use forChild in child route modules!
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AccountRoutingModule {}