import { Component, OnInit, OnDestroy } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Account } from '../account.model'
import { AccountService } from '../account.service'
import { filter, map, tap, switchMap } from 'rxjs/operators'
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-account-detail',
  templateUrl: './account-detail.component.html',
  styleUrls: ['./account-detail.component.css']
})
export class AccountDetailComponent implements OnInit, OnDestroy {
  title = 'account Detail'
  account: Account
  id: number
  subs: Subscription

  constructor(
    private route: ActivatedRoute, 
    private accountService: AccountService) {}

  ngOnInit() {
    // Subscribe to changes in route params.
    // When the route changes we get updates on route params.
    this.subs = this.route.params
      .pipe(
        // we need the 'id' param
        filter(params => params.id),
        // we do not want empty params
        filter(params => !!params),
        // get the number 'id' from the params
        map(params => +params.id),
        // save id locally
        tap(id => (this.id = id)),
        // then wait for users to become available
        switchMap(id => this.accountService.accountsAvailable)
      )
      .subscribe(available => (this.account = this.accountService.getAccount(this.id)))
  }

  ngOnDestroy() {
    if (this.subs) {
      this.subs.unsubscribe()
    }
  }
}