import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountNotfoundComponent } from './account-notfound.component';

describe('AccountNotfoundComponent', () => {
  let component: AccountNotfoundComponent;
  let fixture: ComponentFixture<AccountNotfoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountNotfoundComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountNotfoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
