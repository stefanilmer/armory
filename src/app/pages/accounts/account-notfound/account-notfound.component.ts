import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-account-notfound',
  templateUrl: './account-notfound.component.html',
  styleUrls: ['./account-notfound.component.css']
})
export class AccountNotfoundComponent implements OnInit {

  constructor(private location: Location) { }

  ngOnInit(): void {
  }

  goBack(): void {
    this.location.back();
  }
}
