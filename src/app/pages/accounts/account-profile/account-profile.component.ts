import { Component, OnInit, Input } from '@angular/core'
import { ActivatedRoute, Router, Params } from '@angular/router'
import { AccountService } from '../account.service'
import { AuthService } from '../../../auth/auth.service'
import { Subscription, Observable } from 'rxjs'
import { Account } from '../account.model'

@Component({
  selector: 'app-account-profile',
  templateUrl: './account-profile.component.html',
  styleUrls: ['./account-profile.component.css']
})
export class AccountProfileComponent implements OnInit {
  
  title: string
  id: number
  account: Account
  isLoggedIn$: Observable<boolean>
  isAdmin$: Observable<boolean>
  isActive: boolean

  constructor(
    private route: ActivatedRoute,
    private accountService: AccountService,
    private authService: AuthService) {}

  ngOnInit() {
    this.title = this.route.snapshot.data.title || 'Account Details'
    this.isLoggedIn$ = this.authService.accountIsLoggedIn
    this.isAdmin$ = this.authService.accountIsAdmin

    // Get my account information - get from server, based on JWT token

    //this.accountService.read().subscribe(
    //  account => this.account = new Account(account)
    //)
  }
}
