import { Observable, from } from 'rxjs'
import { map, reduce } from 'rxjs/operators'
import { Pipe, PipeTransform } from '@angular/core'


// Account Roles
export enum AccountRole {
  regular = 0,
  Admin = 1,
}

// String translations
const userRoleNames = ['Regular', 'Admin']

//String translation, translates nr to rolename.
@Pipe({ name: 'asRole' })
export class AccountRoleNamePipe implements PipeTransform {
  transform(roleNr: number) {
    return userRoleNames[roleNr]
  }
}

export enum Gender {
  'male' = 0,
  'female',
  'unknown'
}

export class Account {
  id: string
  gender: Gender = Gender.unknown
  name: {
    firstname: string
    lastname: string
  } = {
    firstname: '',
    lastname: ''
  }
  location: {
    street: { number: string; name: string }
    city: string
    state: string
    postcode: number
  }
  email = ''
  login: {
    uuid: string
    username: string
    password: string
    salt: string
    md5: string
    sha1: string
    sha256: string
  }
  dob: {
    date: Date
    age: number
  }
  registered: {
    date: Date
    age: number
  }
  phone: string
  picture: {
    large: string
    medium: string
    thumbnail: string
  } = {
    large: './assets/images/anonymous-person.png',
    medium: './assets/images/anonymous-person.png',
    thumbnail: './assets/images/anonymous-person.png'
  }
  // nationality
  nat: string
  // Authentication roles
  roles: AccountRole[] = [AccountRole.regular]

  constructor(values: any = {}) {
    // Assign all values to this objects properties
    Object.assign(this, values)
    // In our case, the server returns a different set of name properties.
    // Here we map them to our properties.
    if (values.name && values.name.first) {
      this.name.firstname = values.name.first
    }
    if (values.name && values.name.last) {
      this.name.lastname = values.name.last
    }
    if (values._id) {
      this.id = values._id
    }
  }

  /**
   * Checks whether this user has the given role. Used in authenticated routes.
   *
   * @param rolename UserRole
   */
  public hasRole(rolename: AccountRole): Observable<boolean> {
    return from(this.roles).pipe(
      map(val => val === rolename),
      reduce((a, b) => a || b)
    )
  }

  // Get the user's fullname.
  get fullName() {
    console.log('return', this.name.firstname + ' ' + this.name.lastname)
    return this.name.firstname + ' ' + this.name.lastname
  }
}