import { Component, OnInit, OnDestroy } from '@angular/core'
import { Router } from '@angular/router'
import { Account } from '../account.model'
import { AccountService } from '../account.service'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.css']
})
export class AccountListComponent implements OnInit, OnDestroy {
  title = 'Account ListComponent'
  accounts: Account[]
  subs: Subscription

  constructor(
    private alertService: AlertService, 
    private router: Router, 
    private accountService: AccountService) {}

  ngOnInit() {
    this.subs = this.accountService.getAccounts().subscribe(
      accounts => (this.accounts = accounts),
      error => {
        console.log(error)
        this.alertService.error(
          '<strong>Connection error:</strong> ' + error + '<br/>(Is de server bereikbaar?)'
        )
      }
    )
  }

  ngOnDestroy() {
    if (this.subs) {
      this.subs.unsubscribe()
    }
  }
}
