import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing'
import { AccountListComponent } from './account-list.component'
import { Component, Input, DebugElement, Directive, HostListener } from '@angular/core'
import { AlertService } from '../../../modules/alert/alert.service'
import { Router } from '@angular/router'
import { AccountService } from '../account.service'
import { of } from 'rxjs'
import { By } from '@angular/platform-browser'
import { AccountItemComponent } from './account-item/account-item.component'

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[routerLink]'
})
export class RouterLinkStubDirective {
  @Input('routerLink') linkParams: any
  navigatedTo: any = null

  @HostListener('click')
  onClick() {
    this.navigatedTo = this.linkParams
  }
}

// Global mock object
const dummyAccount = {
  id: 1,
  name: {
    firstname: 'FirstnameTest',
    lastname: 'LastnameTest'
  },
  email: 'test@server.nl',
  picture: { thumbnail: './assets/images/anonymous-person.png' }
}

//
// Test suite for this component
//
describe('AccountListComponent', () => {
  let component: AccountListComponent
  let fixture: ComponentFixture<AccountListComponent>

  let linkDes
  let routerLinks

  let alertServiceSpy: { success: jasmine.Spy; error: jasmine.Spy }
  let routerSpy: { navigateByUrl: jasmine.Spy }
  let accountServiceSpy: { getAccounts: jasmine.Spy }

  beforeEach(async(() => {
    alertServiceSpy = jasmine.createSpyObj('AlertService', ['error', 'success'])
    routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl'])
    accountServiceSpy = jasmine.createSpyObj('AccountService', ['getAccounts'])

    TestBed.configureTestingModule({
      // The declared components needed to test the AccountsComponent.
      declarations: [
        AccountListComponent, // The 'real' component that we will test
        AccountItemComponent,
        RouterLinkStubDirective // Stubbed component required to instantiate the real component.
      ],
      //
      // The constructor of our real component uses dependency injected services that we must mock.
      // NEVER provide the real service in testcases!
      //
      providers: [
        { provide: AlertService, useValue: alertServiceSpy },
        { provide: Router, useValue: routerSpy },
        { provide: AccountService, useValue: accountServiceSpy }
      ]
    }).compileComponents()

    fixture = TestBed.createComponent(AccountListComponent)
    component = fixture.componentInstance
  }))

  afterEach(() => {
    fixture.destroy()
  })

  fit('should create', async(() => {
    //
    // In AccountListComponent.ngOnInit, the AccountService.getAccounts() method is called.
    // We must mock that method and provide an expected result here.
    // Since getAccounts() returns an Observable<Account[]>, we must return a value of that type.
    //
    accountServiceSpy.getAccounts.and.returnValue(of([]))

    // getAccounts() returns an Observable<Account[]>, which is an asynchronous Observable
    // Therefore we have to wait until that subscription returns -> .whenStable().
    fixture.whenStable().then(() => {
      fixture.detectChanges()
      expect(component).toBeTruthy()
      expect(component.accounts.length).toBe(0)
    })
  }))

  it('should display a correct list of accounts', async () => {
    accountServiceSpy.getAccounts.and.returnValue(of([dummyAccount, dummyAccount]))

    // The component subscribes to an asynchronous Observable in ngOnInit, therefore
    // we have to wait until that subscription returns -> .whenStable().
    await fixture.whenStable()
    fixture.detectChanges()
    expect(component).toBeTruthy()
    expect(component.accounts.length).toBe(2)
    //expect(component.accounts[0].name.firstname).toBe('FirstnameTest')
    expect(component.accounts[0].id.toString()).toEqual('1')
    //
    // Optionally add more expects here
    //
  })

  it('should navigate to the correct account details when clicking on a Account row', async(() => {
    accountServiceSpy.getAccounts.and.returnValue(of([dummyAccount]))

    fixture.whenStable().then(() => {
      fixture.detectChanges()

      expect(component).toBeTruthy()
      expect(component.accounts.length).toBe(1)

      // Find elements with an attached RouterLinkStubDirective
      linkDes = fixture.debugElement.queryAll(By.directive(RouterLinkStubDirective))
      // Get attached link directive instances using each DebugElement's injector
      routerLinks = linkDes.map(de => de.injector.get(RouterLinkStubDirective))

      // We should expect TWO routerlinks: 1 for the dummyAccount, AND 1 for the New Account button in the template.
      expect(routerLinks.length).toBe(2, 'should have 2 routerLinks')
      expect(routerLinks[0].linkParams).toBe('new')

      // The second one is the link in the Account list table leading to the AccountDetails route
      // Since we have only 1 Account, and since we use the index to navigate to the Accountdetails,
      // and since we use child routing, the route leads us to the index of our only Account, which is 0.
      expect(routerLinks[1].linkParams[0]).toBe(0)

      // Needs some more research:
      // let AccountLink = fixture.nativeElement.querySelector('a')
      // AccountLink.click()
      // fixture.detectChanges()
      // expect(routerLinks[0].navigatedTo).toBeTruthy() // or .toBe('[0]')
    })
  }))
})