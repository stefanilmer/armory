import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Account } from '../account.model'
import { AccountService } from '../account.service'
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap'

@Component({
  selector: 'app-account-edit',
  templateUrl: './account-edit.component.html',
  styleUrls: ['./account-edit.component.css']
})
export class AccountEditComponent implements OnInit {
  title: string
  // editMode switches between editing existing account or creating a new account.
  // Default is false (= create new account)
  editMode: boolean
  id: number
  account: Account
  submitted = false
  dob

  nationalities = [
    { country: 'Austria', code: 'AT'},
    { country: 'Belgium', code: 'BE'},
    { country: 'Bulgaria', code: 'BG'},
    { country: 'Croatia', code: 'HR'},
    { country: 'Cyprus', code: 'CY'},
    { country: 'Czech', code: 'CZ'},
    { country: 'Denmark', code: 'DK'}, 
    { country: 'Estonia', code: 'EE'},
    { country: 'Finland', code: 'FI'},
    { country: 'France', code: 'FR'},
    { country: 'Germany', code: 'DE'}, 
    { country: 'Greece', code: 'GR'},  
    { country: 'Hungary', code: 'HU'},  
    { country: 'Ireland', code: 'IE'},  
    { country: 'Italy', code: 'IT'},
    { country: 'Latvia', code: 'LV'},  
    { country: 'Lithuania', code: 'LT'}, 
    { country: 'Luxembourg', code: 'LU'}, 
    { country: 'Malta', code: 'MT'}, 
    { country: 'Netherlands', code: 'NL'}, 
    { country: 'Poland', code: 'PL'},  
    { country: 'Portugal', code: 'PT'}, 
    { country: 'Romania', code: 'RO'},  
    { country: 'Slovakia', code: 'SK'}, 
    { country: 'Slovenia', code: 'SI'}, 
    { country: 'Spain', code: 'ES'}, 
    { country: 'Sweden', code: 'SE'}, 
    { country: 'United Kingdom', code: 'GB'}
  ];

  constructor(
    private accountService: AccountService, 
    private route: ActivatedRoute, 
    private router: Router) {}

  ngOnInit() {
    this.title = this.route.snapshot.data.title || 'Edit Account'
    this.editMode = this.route.snapshot.data.accountAlreadyExists || false
    // Get the data statically - changes are not reflected.
    // this.id = +this.route.snapshot.paramMap.get('id');

    if (this.editMode) {
      // Subscribe to changes in route params. When the route changes we get updates on route params.
      this.route.params.subscribe(params => {
        if (params.id) {
          // Subscribe for available accounts. Once accounts are available we get our specific account.
          this.accountService.accountsAvailable.subscribe(accountAvailable => {
            if (accountAvailable) {
              const account = this.accountService.getAccount(+params.id)
              // make local copy of account - detached from original array
              this.account = new Account(JSON.parse(JSON.stringify(account)))
              console.log('Editing', this.account)
              // local date of birth for ngbDatePicker
              this.dob = { year: '1980', month: '10', day: '28' }
            }
          })
        }
      })
    } else {
      // Create a new empty account. The required properties are filled
      // with empty values by the accountmodel.
      this.account = new Account()
    }
  }

  // Save account via the service
  // Then navigate back to display view (= AccountDetails).
  // The display view must then show the new or edited account.
  onSubmit() {
    this.submitted = true
    console.log('onSubmit', this.account)

    if (this.account.id) {
      // A account with id must have been saved before, so it must be an update.
      this.accountService.updateAccount(this.account)
    } else {
      // A account withoud id has not been saved to the database before.
      this.accountService.createAccount(this.account).subscribe(data => console.log(data))
    }

    this.router.navigate(['..'], { relativeTo: this.route })
  }
}

