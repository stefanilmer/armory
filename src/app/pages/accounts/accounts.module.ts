import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { AccountListComponent } from './account-list/account-list.component'
import { AccountDetailComponent } from './account-detail/account-detail.component'
import { AccountEditComponent } from './account-edit/account-edit.component'
import { AccountRoutingModule } from './accounts-routing.module'
import { AccountsComponent } from './accounts.component'
import { AccountItemComponent } from './account-list/account-item/account-item.component'
import { AccountNotfoundComponent } from './account-notfound/account-notfound.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { HttpClientModule } from '@angular/common/http'
import { AccountRoleNamePipe } from './account.model'
import { AccountProfileComponent } from './account-profile/account-profile.component'

@NgModule({
  declarations: [
    AccountsComponent,
    AccountItemComponent,
    AccountListComponent,
    AccountDetailComponent,
    AccountEditComponent,
    AccountProfileComponent,
    AccountNotfoundComponent,
    AccountRoleNamePipe
  ],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, HttpClientModule, NgbModule, AccountRoutingModule],
  providers: [],
  exports: [AccountsComponent]
})
export class AccountsModule {}