import { Component, OnInit } from '@angular/core';
import { UseCase } from '../usecase.model';

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.css'],
})
export class UsecasesComponent implements OnInit {
  readonly PLAIN_USER = 'Reguliere gebruiker';
  readonly ADMIN_USER = 'Administrator';

  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [ 'Gebruiker vult email en password in en klikt op Login knop.',
                  'De applicatie valideert de ingevoerde gegevens.',
                  'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.',],
      actor: this.PLAIN_USER,
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd',
    },
    {
      id: 'UC-02',
      name: 'Registreren',
      description: 'Hiermee kan een (toekomstige)gebruiker zijn account registreren',
      scenario: [ 'De gebruiker heeft nog geen account en klikt op de Registreer knop',
                  'De gebruiker vult een email en wachtwoord in en zijn account word gemaakt.',
                  'Als zijn/haar account successvol gemaakt is wordt hij/zij naar de homepage geredirect.'],
      actor: this.PLAIN_USER,
      precondition: 'De actor heeft geen account',
      postcondition: 'De actor heeft een account gemaakt.',
    },
    {
      id: 'UC-03',
      name: 'Character op account laden',
      description: 'Hiermee kan een gebruiker met een account zijn character op zijn account zetten.',
      scenario: [ 'De gebruiker klikt Account',
                  'De gebruiker klikt op \'Import New Character\' en vult zijn Server en Character naam in ',
                  'De gebruiker klik op \'Import\'.',
                  'De gebruiker wordt naar de Account pagina gestuurd'],
      actor: this.PLAIN_USER,
      precondition: 'De actor heeft een account en logd in',
      postcondition: 'De actor heeft een ingame karakter op zijn account',
    }
  ];

  constructor() {}

  ngOnInit(): void {}
}
