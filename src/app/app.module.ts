import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api'
import { DataService } from './services/data.service'
import { TruncateModule } from '@yellowspot/ng-truncate';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';
import { LayoutComponent } from './layout/layout.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { UsecasesComponent } from './pages/about/usecases/usecases.component';
import { UsecaseComponent } from './pages/about/usecases/usecase/usecase.component';
import { MovieListComponent } from './pages/movie/movie-list/movie-list.component';
import { MovieSearchComponent } from './pages/movie/movie-search/movie-search.component';
import { MovieDetailComponent } from './pages/movie/movie-detail/movie-detail.component';
import { AccountsComponent } from './pages/accounts/accounts.component';
import { AccountEditComponent } from './pages/accounts/account-edit/account-edit.component';
import { AccountProfileComponent } from './pages/accounts/account-profile/account-profile.component';
import { AccountNotfoundComponent } from './pages/accounts/account-notfound/account-notfound.component';
import { AlertComponent } from './modules/alert/alert.component';
import { AccountsModule } from './pages/accounts/accounts.module';
import { AlertModule } from './modules/alert/alert.module';
import { AlertService } from './modules/alert/alert.service';

declare module "@angular/core" {
  interface ModuleWithProviders<T = any> {
    ngModule: Type<T>;
    providers?: Provider[];
  }
}

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    LayoutComponent,
    DashboardComponent,
    UsecasesComponent,
    UsecaseComponent,
    LoginComponent,
    RegisterComponent,
    MovieListComponent,
    MovieSearchComponent,
    MovieDetailComponent,
  ],
  imports: [
    BrowserModule,  
    NgbModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule, 
    TruncateModule,
    AlertModule,
    AccountsModule,
    AppRoutingModule,
    HttpClientInMemoryWebApiModule.forRoot(
      DataService, { dataEncapsulation: false }
    )
  ],
  providers: [AlertService],
  bootstrap: [AppComponent],
})

export class AppModule {}
