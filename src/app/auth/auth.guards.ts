import { Injectable } from '@angular/core'
import { Router, CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'
import { Observable } from 'rxjs'
import { map, take } from 'rxjs/operators'
import { AuthService } from './auth.service'


// Verify Logged in
@Injectable()
export class LoggedInAuthGuard implements CanActivate, CanActivateChild {

    constructor(private router: Router, private authService: AuthService) {}

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        console.log('canActivate LoggedIn')
        const url: string = state.url
        return this.authService.accountIsLoggedIn.pipe(
        take(1),
        map((isLoggedIn: boolean) => isLoggedIn))
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        console.log('canActivateChild LoggedIn')
        return this.canActivate(route, state)
    }
}

// Verify Admin
@Injectable()
export class AdminRoleAuthGuard implements CanActivate, CanActivateChild {
    
    constructor(private router: Router, private authService: AuthService) {}
  
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        console.log('canActivate AdminRole')
        const url: string = state.url
        return this.authService.accountIsAdmin.pipe(
        take(1),
        map((isAdmin: boolean) => isAdmin))
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        console.log('canActivateChild AdminRole')
        return this.canActivate(route, state)
    } 
}