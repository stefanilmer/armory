import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import { Account, AccountRole } from '../pages/accounts/account.model'
import { Router } from '@angular/router'
import { environment } from '../../environments/environment'
import { map, tap, catchError } from 'rxjs/operators'
import { AlertService } from 'src/app/modules/alert/alert.service'
import { HttpClient, HttpHeaders } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class AuthService {
    public isLoggedInAccount = new BehaviorSubject<boolean>(false)
    public loggedInAccountName = new BehaviorSubject<string>('')
    private isAdminAccount = new BehaviorSubject<boolean>(false)
    private isPlainAccount = new BehaviorSubject<boolean>(false)

    private readonly currentAccount = 'currentaccount'
    private readonly currentToken = 'token'

    // store the URL so we can redirect after logging in
    public readonly redirectUrl: string = '/dashboard'
    private readonly headers = new HttpHeaders({ 'Content-Type': 'application/json' })

    constructor(
        private alertService: AlertService, 
        private router: Router, 
        private http: HttpClient) {
            
        this.getcurrentAccount().subscribe({
            next: (account: Account) => {
                console.log(`${account.email} logged in`);
                this.isLoggedInAccount.next(true);
                this.loggedInAccountName.next(account.fullName);
                // Verify the possible roles the Account can have.
                // Add more verifications when nessecary.
                account.hasRole(AccountRole.Admin).subscribe(result => this.isAdminAccount.next(result));
                account.hasRole(AccountRole.regular).subscribe(result => this.isPlainAccount.next(result));
            },
            error: message => {
                this.isLoggedInAccount.next(false);
                this.isAdminAccount.next(false);
                this.isPlainAccount.next(false);
                // this.router.navigate(['/login']);
            }
        });
    }

    login(email: string, password: string) {
        console.log('login')
        console.log(`POST ${environment.ApiUrl}/api/login`)

        return this.http
        .post(`${environment.ApiUrl}/api/login`, { email, password }, { headers: this.headers })
        .pipe(
            tap(
                data => console.log(data),
                error => console.error(error)
            )
        )
        .subscribe({
        next: (response: any) => {
            const currentAccount = new Account(response);
            console.dir(currentAccount);
            this.savecurrentAccount(currentAccount, response.token);

            // Notify all listeners that we're logged in.
            this.isLoggedInAccount.next(true);
            this.loggedInAccountName.next(currentAccount.fullName);
            
            currentAccount.hasRole(AccountRole.Admin).subscribe(result => this.isAdminAccount.next(result));
            currentAccount.hasRole(AccountRole.regular).subscribe(result => this.isPlainAccount.next(result));
            
            this.alertService.success('You have been logged in');
            // If redirectUrl exists, go there
            // this.router.navigate([this.redirectUrl]);
        },
        error: (message: any) => {
            console.log('error:', message);
            this.alertService.error('Invalid credentials');
        }});
    }

    logout() {
        console.log('logout');

        localStorage.removeItem(this.currentAccount);
        localStorage.removeItem(this.currentToken);

        this.isLoggedInAccount.next(false);
        this.isAdminAccount.next(false);
        this.alertService.success('You have been logged out.');
    }

    private getcurrentAccount(): Observable<Account> {
        return new Observable(observer => {
            const localAccount: any = JSON.parse(localStorage.getItem(this.currentAccount));
            console.log('localAccount', localAccount);

            if (localAccount) {
                console.log('localAccount found');
                observer.next(new Account(localAccount));
                observer.complete();
            } else {
                console.log('NO localAccount found');
                observer.error('NO localAccount found');
                observer.complete();
            }
        })
    }

    private savecurrentAccount(Account: Account, token: string): void {
        localStorage.setItem(this.currentAccount, JSON.stringify(Account));
        localStorage.setItem(this.currentToken, token);
    }

    get accountFullName(): Observable<string> {
        console.log('accountFullName ' + this.loggedInAccountName.value);
        return this.loggedInAccountName.asObservable();
    }

    get accountIsLoggedIn(): Observable<boolean> {
        console.log('accountIsLoggedIn() ' + this.isLoggedInAccount.value);
        return this.isLoggedInAccount.asObservable();
    }

    get accountIsAdmin(): Observable<boolean> {
        console.log('accountIsAdmin() ' + this.isAdminAccount.value);
        return this.isAdminAccount.asObservable();
    }

    get accountIsPlain(): Observable<boolean> {
        console.log('accountIsPlain() ' + this.isPlainAccount.value);
        return this.isPlainAccount.asObservable();
    }
}