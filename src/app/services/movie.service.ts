import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment'

import { Movie } from '../interfaces/movie'

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private http: HttpClient) {
    console.log('MovieService constructed')
    console.log(`Connected to ${environment.ApiUrl}`)
   }

  private ApiUrl = 'api/films'; // url to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  /** GET Movies from server */
  getMovies(): Observable<Movie[]> {
    return this.http.get<Movie[]>(this.ApiUrl)
    .pipe(
      tap(_ => console.log('Fetched movies')),
      catchError(this.handleError<Movie[]>('GetMovies', []))
    );
  }

  /**GET Movie by ID, will 404 if no id found */
  getMovie(id: string): Observable<Movie> {
    const url = `${this.ApiUrl}/${id}`;
    return this.http.get<Movie>(url)
    .pipe(
      tap(_ => console.log(`fetched Movie id=${id}`)),
      catchError(this.handleError<Movie>(`GetMovie id=${id}`))
    );
  }

  /** PUT  */
  updateMovie(movie: Movie): Observable<any> {
    return this.http.put(this.ApiUrl, movie, this.httpOptions)
      .pipe(
        tap(_ => console.log(`updated Movie id${movie.id}`)),
        catchError(this.handleError<any>(`updateMovie`))
      );
  }

  addMovie(movie: Movie): Observable<Movie> {
    return this.http.post<Movie>(this.ApiUrl, movie, this.httpOptions)
      .pipe(
        tap((newMovie: Movie) => console.log(`added Movie with id=${newMovie.id}`)),
        catchError(this.handleError<Movie>('addMovie'))
      );
  }

  /** DELETE: delete the account from the server */
  deleteMovie(movie: Movie | number): Observable<Movie> {
    const id = typeof movie === 'number' ? movie : movie.id;
    const url = `${this.ApiUrl}/${id}`;

    return this.http.delete<Movie>(url, this.httpOptions).pipe(
      tap(_ => console.log(`deleted Movie id=${id}`)),
      catchError(this.handleError<Movie>('deleteMovie'))
    );
  }

  /* GET accounts whose name contains search term */
  searchMovie(term: string): Observable<Movie[]> {
    if (!term.trim()) {
      // if not search term, return empty account array.
      return of([]);
    }
    return this.http.get<Movie[]>(`${this.ApiUrl}/?username=${term}`).pipe(
      tap(x => x.length ?
        console.log(`found Movie matching "${term}"`) :
        console.log(`no Movie matching "${term}"`)),
      catchError(this.handleError<Movie[]>('searchMovie', []))
    );
  }

 /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
private handleError<T>(operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {

    // TODO: send the error to remote logging infrastructure
    console.error(error);

    // TODO: better job of transforming error for user consumption
    console.log(`${operation} failed: ${error.message}`);

    // Let the app keep running by returning an empty result.
    return of(result as T);
  }
}
}
