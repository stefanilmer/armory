import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api'

@Injectable({
  providedIn: 'root'
})
export class DataService implements InMemoryDbService {

  constructor() { }

  createDb() {
    const films = [
      {
        ratings: [{
          source: 'FilmGeek',
          value: 'Best 10/10'
          },
          {
            source: 'Rotten Tomatoes',
            value: 'pretty shit 3/10'
          },
          {
            source: 'Stefan',
            value: 'opzich best wel wow, 7/10'
          },
          {
            source: 'Nick',
            value: 'stoer, 9/10'
          }],
        title: 'The Toxic Avenger',
        year: 1984,
        _yearData: '1984',
        rated: 'R',
        released: '1986-04-10T22:00:00.000Z',
        runtime: '82 min',
        genres: 'Action, Comedy, Horror, Sci-Fi',
        director: 'Michael Herz, Lloyd Kaufman',
        writer: 'Lloyd Kaufman (story), Joe Ritter (screenplay), Lloyd Kaufman (additional material), Gay Partington Terry (additional material), Stuart Strutin (additional material)',
        actors: 'Andree Maranda, Mitch Cohen, Jennifer Babtist, Cindy Manion',
        plot: 'This is the story of Melvin, the Tromaville Health Club mop boy, who inadvertently and naively trusts the hedonistic, contemptuous and vain health club members, to the point of accidentally ending up in a vat of toxic waste. The devastating results then have a transmogrification effect, his alter ego is released, and the Toxic Avenger is born, to deadly and comical results. The local mop boy is now the local Superhero, the saviour of corruption, thuggish bullies and indifference. Troma classic with good make-up effects and stunts, a pleasant surprise indeed.',
        languages: 'English',
        country: 'USA',
        awards: '1 nomination.',
        poster: 'https://m.media-amazon.com/images/M/MV5BNzViNmQ5MTYtMmI4Yy00N2Y2LTg4NWUtYWU3MThkMTVjNjk3XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg',
        metascore: '42',
        rating: 6.3,
        votes: '24,888',
        id: 'tt0090190',
        type: 'movie',
        dvd: undefined,
        boxoffice: 'N/A',
        production: 'Troma Entertainment Inc.',
        website: 'N/A',
        name: 'The Toxic Avenger',
        series: false,
        imdburl: 'https://www.imdb.com/title/tt0090190'
      },
      {
        title: 'Borat: Cultural Learnings of America for Make Benefit Glorious Nation of Kazakhstan',
        year: 2006,
        _yearData: '2006',
        rated: 'R',
        released: '2006-11-02T23:00:00.000Z',
        runtime: '84 min',
        genres: 'Comedy',
        director: 'Larry Charles',
        writer: 'Sacha Baron Cohen (screenplay), Anthony Hines (screenplay), Peter Baynham (screenplay), Dan Mazer (screenplay), Sacha Baron Cohen (story), Peter Baynham (story), Anthony Hines (story), Todd Phillips (story), Sacha Baron Cohen (based on a character created by)',
        actors: 'Sacha Baron Cohen, Ken Davitian, Luenell, Chester',
        plot: "Borat Sagdiyev is a TV reporter of a popular show in Kazakhstan as Kazakhstan's sixth most famous man and a leading journalist. He is sent from his home to America by his government to make a documentary about American society and culture. Borat takes a course in New York City to understand American humor. While watching Baywatch on TV, Borat discovers how beautiful their women are in the form of C. J. Parker, who was played by actress Pamela Anderson who hails from Malibu, California. He decides to go on a cross-country road trip to California in a quest to make her his wife and take her back to his country. On his journey Borat and his producer encounter a country full of strange and wonderful Americans, real people in real chaotic situations with hysterical consequences.",
        languages: 'English, Romanian, Hebrew, Polish, Armenian',
        country: 'USA, UK',
        awards: 'Nominated for 1 Oscar. Another 20 wins & 33 nominations.',
        poster: 'https://m.media-amazon.com/images/M/MV5BMTk0MTQ3NDQ4Ml5BMl5BanBnXkFtZTcwOTQ3OTQzMw@@._V1_SX300.jpg',
        metascore: '89',
        rating: 7.3,
        votes: '372,069',
        id: 'tt0443453',
        type: 'movie',
        dvd: undefined,
        boxoffice: 'N/A',
        production: 'Gold/Miller Productions',
        website: 'N/A',
        name: 'Borat: Cultural Learnings of America for Make Benefit Glorious Nation of Kazakhstan',
        series: false,
        imdburl: 'https://www.imdb.com/title/tt0443453'
      },
      {
        title: 'It',
        year: 2017,
        _yearData: '2017',
        rated: 'R',
        released: '2017-09-07T22:00:00.000Z',
        runtime: '135 min',
        genres: 'Horror',
        director: 'Andy Muschietti',
        writer: 'Chase Palmer (screenplay by), Cary Joji Fukunaga (screenplay by), Gary Dauberman (screenplay by), Stephen King (based on the novel by)',
        actors: 'Jaeden Martell, Jeremy Ray Taylor, Sophia Lillis, Finn Wolfhard',
        plot: "In the Town of Derry, the local kids are disappearing one by one, leaving behind bloody remains. In a place known as 'The Barrens', a group of seven kids are united by their horrifying and strange encounters with an evil clown and their determination to kill It.",
        languages: 'English, Hebrew',
        country: 'Canada, USA',
        awards: '8 wins & 49 nominations.',
        poster: 'https://m.media-amazon.com/images/M/MV5BZDVkZmI0YzAtNzdjYi00ZjhhLWE1ODEtMWMzMWMzNDA0NmQ4XkEyXkFqcGdeQXVyNzYzODM3Mzg@._V1_SX300.jpg',
        metascore: '69',
        rating: 7.3,
        votes: '450,627',
        id: 'tt1396484',
        type: 'movie',
        dvd: undefined,
        boxoffice: 'N/A',
        production: 'Vertigo Entertainment, RatPac-Dune Entertainment, Lin Pictures',
        website: 'N/A',
        name: 'It',
        series: false,
        imdburl: 'https://www.imdb.com/title/tt1396484'
      }
    ];

    return {films};
  }
}
