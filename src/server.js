const express = require("express");
const path = require("path");
const http = require("http");
const compression = require("compression");
const bodyParser = require('body-parser');
const app = express();
const imdb = require('imdb-api');

app.use(compression());

// Body Parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const appname = "IMDB APP";
const ApiKey = "596d5bc7";

// Point static path to dist
app.use(express.static(path.join(__dirname, "..", "dist", appname)));

// Catch all routes and return the index file
app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "..", "dist", appname, "index.html"));
});

// Get port from environment and store in Express.
const port = process.env.PORT || "4200";
app.set("port", port);

// Create HTTP server.
const server = http.createServer(app);

// Listen on provided port, on all network interfaces.
server.listen(port, () => {
  console.log(`Angular app \'${appname}\' running in ${process.env.NODE_ENV} mode on port ${port}`);
  imdb.get({name: 'it'}, {apiKey: ApiKey, timeout: 30000}).then(console.log).catch(console.log);
});
